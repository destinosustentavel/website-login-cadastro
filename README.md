# Login e Cadastro

Login e Cadastro do website do Destino Sustentável.

## Ferramentas utilizadas

* [ReactJS](https://pt-br.reactjs.org/)
* [Yarn](https://yarnpkg.com/)

## Como utilizar

```bash
# clone este repositório
git clone https://gitlab.com/destinosustentavel/website-login-cadastro.git

# instale as dependências
npm install / yarn install

# execute a aplicação
npm start / yarn start
```