import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

import api from '../../services/api';

import Container from '../../components/Container';
import Checkbox from '../../components/Checkbox';
import Button from '../../components/Button';
import Input from '../../components/Input';

import './styles.css';

export default function Login() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  useEffect(() => {
    const loginData = JSON.parse(localStorage.getItem('loginData'));

    if(loginData) finishLogin(loginData.email, loginData.password);
  }, []);

  function rememberMe(checked) {
    if(checked) {
      const loginData = { email, password };
      localStorage.setItem('loginData', JSON.stringify(loginData));
    }
  }

  function finishLogin(loginEmail, loginPassword) {
    api.post('login', {
      email: loginEmail,
      password: loginPassword
    });
  }

  return (
    <Container pageTitle="Login">
      <div className="login-inputs">
        <Input
          type="text"
          title="E-mail"
          onChange={e => setEmail(e.target.value)}
        />
        
        <Input
          type="password"
          title="Senha"
          onChange={e => setPassword(e.target.value)}
        />
      </div>

      <div className="actions">
        <Checkbox
          title="Lembrar-me"
          nameFor="remember-me"
          onChangeEvent={e => rememberMe(e.target.checked)}
        />

        <Link className="password-button" to="/recuperar-senha">
          Esqueceu a senha?
        </Link>
      </div>
      
      <div className="buttons-container">
        <Button
          title="Entrar"
          onClickEvent={() => finishLogin(email, password)}
        />

        <Link className="signup-button" to="/cadastro">
          Não possui cadastro?
        </Link>
      </div>
    </Container>
  );
}