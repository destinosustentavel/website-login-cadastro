import React, { useState, useEffect } from 'react';
import { FiCheck, FiX, FiEye, FiEyeOff } from 'react-icons/fi';

import Container from '../../components/Container';
import Button from '../../components/Button';
import Input from '../../components/Input';

import './styles.css';

export default function ResetPassword() {
  const [newPassword, setNewPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [isSamePassword, setIsSamePassword] = useState('');

  const [eyeNewOpen, setEyeNewOpen] = useState(true);
  const [eyeConfirmOpen, setEyeConfirmOpen] = useState(true);

  useEffect(() => {
    const isPasswordEqual = confirmPassword === newPassword ? true : false;

    setIsSamePassword(isPasswordEqual);
  }, [newPassword, confirmPassword]);

  function tooglePasswordVisibility(id) {
    const input = document.getElementById(id);
    const inputType = input.type === 'password' ? 'text' : 'password';
    input.type = inputType;

    if(id === 'new-password') {
      if(inputType === 'password') {
        setEyeNewOpen(true);
      } else {
        setEyeNewOpen(false);
      }
    } else {
      if(inputType === 'password') {
        setEyeConfirmOpen(true);
      } else {
        setEyeConfirmOpen(false);
      }
    }
  }

  function defineNewPassword() {
    if (isSamePassword) {
      console.log(newPassword);
    }
  }

  return (
    <Container pageTitle="Redefinir Senha">
      <div className="reset-inputs">
        <div className="password-input">
          <Input
            id="new-password"
            type="password"
            title="Nova Senha"
            onChange={e => setNewPassword(e.target.value)}
          />

          <div className="eye-icon">
          {
            eyeNewOpen ? (
              <FiEye
                size={22}
                color="#fff"
                onClick={() => tooglePasswordVisibility('new-password')}
              />
            ) : (
              <FiEyeOff
                size={22}
                color="#fff"
                onClick={() => tooglePasswordVisibility('new-password')}
              />
            )
            }
          </div>
        </div>

        <div className="password-input">
          <Input
            id="confirm-password"
            type="password"
            title="Confirmar Senha"
            onChange={e => setConfirmPassword(e.target.value)}
          />

          <div className="eye-icon">
            {
              eyeConfirmOpen ? (
                <FiEye
                  size={22}
                  color="#fff"
                  onClick={() => tooglePasswordVisibility('confirm-password')}
                />
              ) : (
                <FiEyeOff
                  size={22}
                  color="#fff"
                  onClick={() => tooglePasswordVisibility('confirm-password')}
                />
              )
            }
          </div>
        </div>
      </div>

      {
        newPassword && confirmPassword && isSamePassword &&
        <div className="same-password">
          <FiCheck size={25} color="#0f0" />
          <span>As senhas são iguais</span>
        </div>
      }

      {
        newPassword && confirmPassword && !isSamePassword &&
        <div className="same-password">
          <FiX size={25} color="#f00" />
          <span>As senhas são diferentes</span>
        </div>
      }

      <Button title="Finalizar" onClickEvent={defineNewPassword} />
    </Container>
  );
}