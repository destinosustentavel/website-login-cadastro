import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';

import api from '../../services/api';

import AskLocation from '../../components/AskLocation';
import Container from '../../components/Container';
import Checkbox from '../../components/Checkbox';
import Button from '../../components/Button';
import Input from '../../components/Input';

import './styles.css';

export default function Signup() {
  const [username, setUsername] = useState('');
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [cnpj, setCnpj] = useState('');
  const [cep, setCep] = useState('');
  const [phone, setPhone] = useState('');
  const [interest, setInterest] = useState('');
  const [userType, setUserType] = useState('');

  const [latitude, setLatitude] = useState(0);
  const [longitude, setLongitude] = useState(0);
  const [altitude, setAltitude] = useState(0);

  const [askLocationScreen, setAskLocationScreen] = useState('none');

  const history = useHistory();

  function getLocation() {
    navigator.geolocation.getCurrentPosition(({ coords }) => {
      setLatitude(coords.latitude);
      setLongitude(coords.longitude);
      setAltitude(coords.altitude);

      setAskLocationScreen('none');
    });
  }

  useEffect(() => {
    getLocation();

    navigator.permissions.query({ name: 'geolocation' }).then(result => {
      if (result.state === 'prompt') {
        setAskLocationScreen('flex');
        getLocation();
      }
      
      result.onchange = () => {
        setAskLocationScreen('none');
      }
    });
  }, []);

  function finishSignup() {
    const userData = {
      username,
      name,
      email,
      password,
      PJ_associated: cnpj,
      cep,
      phone,
      interest,
      type: userType,
      latitude,
      longitude,
      altitude
    };

    const inputCheck = [...document.querySelectorAll('input[type=checkbox]')];
    const verifyChecked = inputCheck.map(input => input.checked);

    if (!verifyChecked[0] || !verifyChecked[1]) {
      alert('Aceite os termos de uso e a política de privacidade da plataforma para continuar');
    } else {
      api.post('users/register', userData);
    }
  }

  return (
    <>
      <AskLocation visibility={askLocationScreen} />

      <Container pageTitle="Cadastro">
        <div className="signup-inputs">
        <Input
            type="text"
            title="Nome *"
            required={true}
            onChange={e => setName(e.target.value)}
          />

          <Input
            type="text"
            title="Nome de usuário *"
            required={true}
            onChange={e => setUsername(e.target.value)}
          />

          <Input
            type="text"
            title="E-mail *"
            required={true}
            onChange={e => setEmail(e.target.value)}
          />

          <Input
            type="password"
            title="Senha *"
            required={true}
            onChange={e => setPassword(e.target.value)}
          />

          <Input
            type="text"
            title="CNPJ"
            required={true}
            onChange={e => setCnpj(e.target.value)}
          />

          <Input
            type="text"
            title="CEP *"
            required={true}
            onChange={e => setCep(e.target.value)}
          />

          <Input
            type="text"
            title="Telefone"
            required={false}
            onChange={e => setPhone(e.target.value)}
          />

          <Input
            type="text"
            title="Interesse"
            required={false}
            onChange={e => setInterest(e.target.value)}
          />

          <Input
            type="text"
            title="Tipo"
            required={false}
            onChange={e => setUserType(e.target.value)}
          />
        </div>

        <div className="inputs-terms-container">
          <Checkbox
            nameFor="use-terms"
            title="Eu aceito os termos de uso da plataforma"
          />

          <Checkbox
            nameFor="privacy-policy"
            title="Eu aceito a política de privacidade da plataforma"
          />
        </div>

        <Button
          title="Confirmar"
          onClickEvent={finishSignup}
        />

        <Button
          title="Termos de Uso"
          onClickEvent={() => history.push('termos')}
        />
      </Container>
    </>
  );
}