import React, { useState } from 'react';

import Container from '../../components/Container';
import Button from '../../components/Button';
import Input from '../../components/Input';

import './styles.css';

export default function RecoverPassword() {
  const [email, setEmail] = useState('');

  function sendEmail() {
    console.log(email);
  }

  return (
    <Container pageTitle="Recuperar Senha">
      <Input
        type="text"
        title="Digite seu E-mail"
        onChange={e => setEmail(e.target.value)}
      />

      <Button title="Enviar E-mail" onClickEvent={sendEmail} />
    </Container>
  );
}