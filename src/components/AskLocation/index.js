import React from 'react';

import icon from '../../images/icon.png';

import './styles.css';

export default function AskLocation({ visibility }) {
  return (
    <div className="asklocation-container" style={{ display: visibility }}>
      <h1>Permita a localização</h1>

      <div>
        <span>Clique em "Permitir"</span>
        <span>ou</span>
        <span>Clique no ícone na barra de endereço indicado na imagem abaixo e permita</span>
        <img src={icon} alt="Location"/>
      </div>
    </div>
  );
}