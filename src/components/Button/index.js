import React from 'react';

import './styles.css';

export default function Button({ title, onClickEvent }) {
  return (
    <button className="action-button" onClick={onClickEvent}>{title}</button>
  );
}