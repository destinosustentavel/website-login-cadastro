import React from 'react';

import './styles.css';

export default function Input({ id, title, type, required,onChange }) {
  return (
    <div className="input-container">
      <span>{title}</span>

      <input
        id={id}
        type={type}
        required={required}
        onChange={onChange}
      />
    </div>
  );
}