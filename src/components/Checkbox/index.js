import React from 'react';

import './styles.css';

export default function Checkbox({ title, nameFor, onChangeEvent }) {
  return (
    <div className="checkbox-container">
      <input type="checkbox" id={nameFor} onChange={onChangeEvent} />
      <label htmlFor={nameFor}>{title}</label>
    </div>
  );
}