import React from 'react';

import logo from '../../images/logo.svg';

import './styles.css';

export default function Container({ children, pageTitle }) {
  return (
    <div className="container">
      <img src={logo} alt="Destino Sustentável"/>

      <h2>{pageTitle}</h2>

      {children}
    </div>
  );
}