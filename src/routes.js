import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';

import Login from './pages/Login';
import Signup from './pages/Signup';
import RecoverPassword from './pages/RecoverPassword';
import ResetPassword from './pages/ResetPassword';
import Terms from './pages/Terms';

export default function Routes() {
  return (
    <BrowserRouter>
      <Route path="/" exact component={Login} />
      <Route path="/cadastro" component={Signup} />
      <Route path="/recuperar-senha" component={RecoverPassword} />
      <Route path="/redefinir-senha" component={ResetPassword} />
      <Route path="/termos" component={Terms} />
    </BrowserRouter>
  );
}